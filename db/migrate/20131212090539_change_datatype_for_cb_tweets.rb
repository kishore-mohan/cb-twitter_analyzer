class ChangeDatatypeForCbTweets < ActiveRecord::Migration
  def change
    change_column :cb_tweets, :tweet_id, :string
  end
end
