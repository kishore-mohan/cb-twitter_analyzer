class CbTweet < ActiveRecord::Migration
  def change
    create_table(:cb_tweets) do |t|
      t.integer :tweet_id
      t.text :tweet_text,:default => ""
      t.string :tweet_user_id
      t.string :tweet_user_name
      t.string :tweet_user_location
      t.string :followers_count
      t.string :friends_count
      t.string :profile_img_url
      t.string :retweet_count
      t.integer :contributor_id
    end
    add_index :cb_tweets, :tweet_id, :unique => true
    add_index :cb_tweets, :contributor_id
  end
end
