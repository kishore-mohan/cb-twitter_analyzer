# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20131212155935) do

  create_table "cb_tweets", :force => true do |t|
    t.string   "tweet_id"
    t.text     "tweet_text",          :default => ""
    t.string   "tweet_user_id"
    t.string   "tweet_user_name"
    t.string   "tweet_user_location"
    t.string   "followers_count"
    t.string   "friends_count"
    t.string   "profile_img_url"
    t.string   "retweet_count"
    t.integer  "contributor_id"
    t.datetime "tweet_time"
    t.string   "user_url"
  end

  add_index "cb_tweets", ["contributor_id"], :name => "index_cb_tweets_on_contributor_id"
  add_index "cb_tweets", ["tweet_id"], :name => "index_cb_tweets_on_tweet_id", :unique => true

  create_table "contributors", :force => true do |t|
    t.string "name"
  end

end
