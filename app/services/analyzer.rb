require 'twitter'
require 'hashie'

class Analyzer
  def initialize(contributor_id,contributor_name,since_id)
    @client = Twitter::REST::Client.new do |config|
      config.consumer_key = Settings.tweet_stream.consumer_key
      config.consumer_secret = Settings.tweet_stream.consumer_secret
      config.access_token = Settings.tweet_stream.oauth_token
      config.access_token_secret = Settings.tweet_stream.oauth_token_secret
    end
    #@date = date
    @contributor_id=contributor_id
    @contributor_name = contributor_name
    @since_id = since_id
  end


  def stop
    @client.stop
  end

  def start

    unless @contributor_name.blank?
    params ={:result_type => "recent",
               :@since_id => @since_id,:count=>100 }.reject { |k, v| v.blank?}
      response= @client.search(@contributor_name,
                     params)

      response.collect do |tweet|
        cbtweeting = CbTweet.where(:tweet_id => tweet.id.to_s).first
        if cbtweeting.nil?
          cb=CbTweet.new
          cb.tweet_time = tweet.created_at
          cb.tweet_id = tweet.id.to_s
          cb.contributor_id = @contributor_id
          cb.tweet_text = tweet.text
          cb.tweet_user_id = tweet.user.id
          cb.tweet_user_name = tweet.user.name
          cb.tweet_user_location = tweet.user.location || twitter.time_zone
          cb.followers_count = tweet.user.followers_count
          cb.friends_count = tweet.user.friends_count
          cb.profile_img_url = tweet.user.profile_image_url.to_s
          cb.retweet_count = tweet.retweet_count
          cb.tweet_time =tweet.created_at
          cb.user_url = tweet.user.url.to_s
          cb.save
        else
          #cbtweeting.update_attributes(:user_url=>tweet.user.url.to_s)
        end
      end
    end
    end


end

