#ENV['RAILS_ENV'] = ARGV.first || ENV['RAILS_ENV'] || 'development'
#require File.expand_path(File.dirname(__FILE__) + "/../../config/environment")
class StartTweetAnalyser
  include Sidekiq::Worker
  sidekiq_options({
                      :queue => :worker ,
                      # Should be set to true (enables uniqueness for async jobs)
                      # or :all (enables uniqueness for both async and scheduled jobs)
                      :unique => :all
                  })
 def perform
  @contributors= Contributor.all.each do |s|
   since_id=CbTweet.where(:contributor_id => s.id).pluck(:tweet_id).last
   Analyzer.new(s.id , s.name,since_id).start
  end
  end
  end
