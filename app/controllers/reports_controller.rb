class ReportsController < ApplicationController
  # GET /homes
  # GET /homes.json
  def index
    @h = LazyHighCharts::HighChart.new('graph') do |f|
      f.chart(:renderTo => 'container', :zoomType => 'x',:spacingRight=> 20)
      f.title(:text => 'Activity')
      f.xAxis(:title=>{:text => 'Time'}, :categories =>CbTweet.time_gap)
      f.yAxis(:title=>{:text=> 'Tweets Count', :type =>'integer' ,:max => 5000000})
      f.series(:name =>'Tweets', :data=>CbTweet.tweet_count,:color=>"#0AA30F")
    end
    @total_impressions = CbTweet.sum_followers_count + CbTweet.sum_friends_count
    @estimated_reach  = CbTweet.sum_followers_count
    @total_tweets = CbTweet.total_tweets
    @total_contributor = CbTweet.total_contributor

    @bar = LazyHighCharts::HighChart.new('column') do |f|
      f.chart(:type => 'column', :zoomType => 'x',:spacingRight=> 20)
      f.xAxis(:title=>{:text => 'Bars show number of tweets sent by users with that many followers '}, :categories =>["<100","<1k","<10k","100k","100k+" ])
      f.yAxis(:title=>{:text=> 'Tweets Count', :align =>'high'} ,:labels=>{:overflow=>'justify'})
      f.series(:name=>'Tweets',:data=> CbTweet.tweet_impression,:color=>"#C2663D")
      f.title({ :text=>"Exposure"})
      f.legend(:align => 'right', :verticalAlign => 'top', :y => 75, :x => -50, :layout => 'vertical',)
      ###  Options for Bar
      f.options[:chart][:defaultSeriesType] = "bar"
      f.plot_options({:series=>{:stacking=>"normal"}})

    end
    @chart= LazyHighCharts::HighChart.new('pie') do |f|
      f.chart({:defaultSeriesType=>"pie" , :margin=> [20, 120, 20, 110]} )
      series = {
          :type=> 'pie',
          :name=> 'Tweets',
          :data=> CbTweet.pie_tweet,
          :colors=> ["#FF7D82","#F2FF33"]
      }
      f.series(series)
      f.options[:title][:text] = "Tweets Activity"
      f.legend(:layout=> 'vertical',:style=> {:left=> 'auto', :bottom=> 'auto',:right=> '50px',:top=> '100px'})
      f.plot_options(:pie=>{
          :allowPointSelect=>true,
          :cursor=>"pointer" ,
          :dataLabels=>{
              :enabled=>true,
              :color=>"black",
              :style=>{
                  :font=>"13px Trebuchet MS, Verdana, sans-serif"
              }
          }
      })
      end
    @most_retweets=CbTweet.most_retweet.order("retweet_count desc").limit(3)
    @tweet_timelines = CbTweet.most_retweet.limit(30)
    @top_contributors = CbTweet.top_contributor.first(30)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @contributors }
    end
  end

  def new

  end

  def tweet_analyse
    @contributor = Contributor.where(:name=>params[:q]).first_or_create
    unless @contributor.nil?
    Analyzer.new(@contributor.id , @contributor.name,nil).start
   end
    @line_charts = LazyHighCharts::HighChart.new('graph') do |f|
      f.chart(:renderTo => 'container', :zoomType => 'x',:spacingRight=> 20)
      f.title(:text => 'Activity')
      f.xAxis(:title=>{:text => 'Time'}, :categories =>CbTweet.time_gap)
      f.yAxis(:title=>{:text=> 'Tweets Count', :type =>'integer' ,:max => 5000000})
      f.series(:name =>'Tweets', :data=>CbTweet.contributor_tweet_count(@contributor.id),:color=>"#0AA30F")
    end
    @total_impressions = CbTweet.where(:contributor_id => @contributor.id).sum_followers_count + CbTweet.where(:contributor_id => @contributor.id).sum_friends_count
    @estimated_reach  = CbTweet.where(:contributor_id => @contributor.id).sum_followers_count
    @total_tweets = CbTweet.where(:contributor_id => @contributor.id).count


  end

  def show

  end
end
