class CbTweet < ActiveRecord::Base
  attr_accessible :since_tweet_id, :tweet_text, :tweet_user_location, :tweet_user_name,
                  :retweet_count, :tweet_user_id,:followers_count,:friends_count,:profile_img_url,
                  :contributor_id, :tweet_time , :user_url
  belongs_to :contributor

  def self.maxtime
    Time.zone.parse((self.maximum(:tweet_time) - 6.hours).to_s)
  end

  def self.time_gap
    a=[]
    (time_roundup.to_time.to_i.. self.maximum(:tweet_time).to_time.to_i).step(30.minute) do |s|
      a << Time.at(s).strftime("%H:%M")
    end
    return a
  end

  def self.time_roundup
    self.maxtime.sec.seconds.ago(((self.maxtime.min).minutes.ago(self.maxtime)))
  end

  def self.tweet_count
    max_time= self.maximum(:tweet_time)
    a= []
    ((max_time-6.hours).to_time.to_i .. (max_time.to_time.to_i)).step(30.minute) do |date|
      a << self.where("tweet_time >= ? and tweet_time < ? " , Time.at(date)-30.minute , Time.at(date)).count(:tweet_id)
    end
    return a
  end

  def self.sum_friends_count
    self.uniq.sum("friends_count")
  end

  def self.sum_followers_count
    self.uniq.sum("followers_count")
  end

  def self.tweet_impression
    imp = []
    [[1..100],[100..1000],[1000..10000],[10000..100000],[100000..100000000]].each do |s|
      imp << where(:followers_count=>s).count
    end
    return imp
  end

  def self.pie_tweet
    retweet = find_by_sql("select sum(retweet_count) as retweet,count(*) as c from cb_tweets where tweet_time <= '#{maxtime.to_s}'")
    return [["Tweets",retweet.first.c.to_i],["Retweet",retweet.first.retweet.to_i]]
  end

  def self.total_tweets
    where("tweet_time >=?",maxtime).count
  end

  def self.total_contributor
    where("tweet_time >= ?",maxtime).pluck(:tweet_user_name).uniq.count
  end

  def self.most_retweet
    where("tweet_time >= ?",maxtime)
  end

  def self.top_contributor
    find_by_sql("select tweet_user_name as user,followers_count as follow,count(*) as c  from cb_tweets group by tweet_user_name,followers_count ;").sort_by!{|x|  -(x.follow.to_i)}
  end

  def self.contributor_tweet_count(id)
    max_time= self.where(:contributor_id => id).maximum(:tweet_time)
    a= []
    unless max_time.nil?
      ((max_time-6.hours).to_time.to_i .. (max_time.to_time.to_i)).step(30.minute) do |date|
        a << self.where("tweet_time >= ? and tweet_time < ? and contributor_id =?" , Time.at(date)-30.minute , Time.at(date) ,id).count(:tweet_id)
      end
    end
    return a
  end


end
