class Contributor < ActiveRecord::Base
  attr_accessible :name
  validates_presence_of(:name)
  has_many :cb_tweets , dependent: :destroy
end
